# Ian Pedowitz's dotfiles

## Purpose

This project contains my `dotfiles`, my local `~/bin/` directory, my RectangleConfig.json, and some screenshots to help me set up a new computer.

## Things to install/do if I swap a machine

1. [Chrome](https://www.google.com/chrome/downloads/)
1. [Chrome Canary](https://www.google.com/chrome/canary/)
1. [Okta Verify](https://apps.apple.com/ca/app/okta-verify/id490179405)
1. [1Password](https://1password.com/mac/)
1. [Rectangle](https://rectangleapp.com/)
1. [Slack](https://slack.com/downloads/mac)
1. [Zoom](https://zoom.us/client/latest/zoomusInstallerFull.pkg?archType=arm64)
1. [NordLayer](https://nordlayer.com/download/)
1. [Oh My Zsh](https://ohmyz.sh/)
1. [GitLab Handbook](https://about.gitlab.com/handbook/git-page-update/#3-the-single-script-setup-method-macos-only)
1. [`brew`](https://brew.sh/)
1. [`glab`](https://gitlab.com/gitlab-org/cli#installation)
1. [`gdk`](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#one-line-installation)
   - Make sure to edit the `gdk.yml` file with [this hook](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/troubleshooting/postgresql.md#gdk-update-leaves-gitlabdb-with-uncommitted-changes)
1. [VS Code](https://code.visualstudio.com/)
1. Setup SSH access to GitLab via 1Password
   1. https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair-with-1password
   1. https://developer.1password.com/docs/ssh/get-started/
1. Install GitLab Workflow extension for VS Code
   1. https://docs.gitlab.com/ee/editor_extensions/visual_studio_code/
   1. https://marketplace.visualstudio.com/items?itemName=gitlab.gitlab-workflow
1. [Xcode](https://apps.apple.com/us/app/xcode/id497799835)
1. Xcode Command Line Tools - `xcode-select –install`
1. [Android Studio](https://developer.android.com/studio)
1. [HP Smart](https://apps.apple.com/app/hp-smart/id1474276998)
1. [Lithionics Battery Monitor](https://apps.apple.com/us/app/lithionics-battery-monitor/id1437635365)
1. [Signal](https://signal.org/download/)
1. [VLC](https://www.videolan.org/vlc/)

## Repositories I have checked out

1. [Product](https://gitlab.com/gitlab-com/Product)
1. [buyer-experience](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience)
1. [customers-gitlab-com](https://gitlab.com/gitlab-org/customers-gitlab-com)
1. [docsy-gitlab](https://gitlab.com/gitlab-com/content-sites/docsy-gitlab)
1. [dotfiles](https://gitlab.com/ipedowitz/dotfiles)
1. [engineering](https://gitlab.com/gitlab-com/engineering-division/engineering)
1. [finance-systems](https://gitlab.com/gitlab-com/business-technology/enterprise-apps/financeops/finance-systems)
1. [gitlab](https://gitlab.com/gitlab-org/gitlab)
1. [gitlab-api-automated-commenter](https://gitlab.com/gitlab-de/use-cases/gitlab-api/gitlab-api-automated-commenter)
1. [gitlab-docs](https://gitlab.com/gitlab-org/gitlab-docs)
1. [gitlab-development-kit](https://gitlab.com/gitlab-org/gitlab-development-kit)
1. [handbook](https://gitlab.com/gitlab-com/content-sites/handbook)
1. [internal-handbook](https://gitlab.com/gitlab-com/content-sites/internal-handbook)
1. [ipedowitz](https://gitlab.com/ipedowitz/ipedowitz)
1. [maintenance-tasks](https://gitlab.com/gitlab-com/content-sites/handbook-tools/maintenance-tasks)
1. [triage-ops](https://gitlab.com/gitlab-org/quality/triage-ops)
1. [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com)
1. `mkdir tmp` for temporary repo's