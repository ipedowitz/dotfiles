#!/bin/zsh

# Set the root directory to ~/GitLab.com
root_dir=~/GitLab.com

# Recursively find all subdirectories in the root directory
find "$root_dir" -type d -name .git | sort | while read -r dir; do
  # Get the parent directory of the .git folder
  repo_dir=$(dirname "$dir")

  # Check if the repository directory contains "_build/deps"
  if [[ "$repo_dir" == *"_build/deps"* ]]; then
    continue
  fi

  # Change into the repository directory
  cd "$repo_dir"

  # Print the working directory
  echo "Working directory: $(pwd)"

  # Run `git branch`
  git branch

  # Add a newline
  echo

  # Change back to the root directory
  cd "$root_dir"
done
