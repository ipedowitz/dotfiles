eval "$(/opt/homebrew/bin/brew shellenv)"

. /opt/homebrew/opt/asdf/libexec/asdf.sh
autoload -Uz compinit && compinit

# . /opt/homebrew/opt/asdf/asdf.sh
# . $(brew --prefix)/opt/asdf/libexec/asdf.sh

if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

if [[ -d "${HOME}/Library/Android/sdk/platform-tools/" ]]; then
  export PATH=$PATH:${HOME}/Library/Android/sdk/platform-tools/
fi

if [[ -d "${HOME}/Library/Android/sdk/build-tools/34.0.0-rc4/" ]]; then
  export PATH=$PATH:${HOME}/Library/Android/sdk/build-tools/34.0.0-rc4/
fi

source /Users/ipedowitz/.config/op/plugins.sh
